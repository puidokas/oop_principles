﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solid_principles.principles
{
    class DIP : IRunnable
    {
        public string Name
        {
            get
            {
                return "Dependency Inversion Principle (DIP)";
            }
        }

        public void Run()
        {
            LightBulb lightBulb = new LightBulb();

            Switch switchDevice = new Switch(lightBulb);

            Console.WriteLine($"Switching device...");
            for (int i = 0; i < 10; i++)
            {
                switchDevice.Operate();
            }
        }
    }

    // Without DIP
    //public class LightBulb
    //{
    //    public void TurnOn()
    //    {
    //        // Turn on logic
    //    }

    //    public void TurnOff()
    //    {
    //        // Turn off logic
    //    }
    //}

    //public class Switch
    //{
    //    private LightBulb bulb;

    //    public Switch()
    //    {
    //        bulb = new LightBulb();
    //    }

    //    public void Operate()
    //    {
    //        // Operate switch logic
    //        if (/* some condition */)
    //        {
    //            bulb.TurnOn();
    //        }
    //        else
    //        {
    //            bulb.TurnOff();
    //        }
    //    }
    //}

    // Applying DIP
    public interface ISwitchable
    {
        void TurnOn();
        void TurnOff();
    }

    public class LightBulb : ISwitchable
    {
        public void TurnOn()
        {
            // Turn on logic
            Console.WriteLine($"{GetType().Name} is turned on");
        }

        public void TurnOff()
        {
            // Turn off logic
            Console.WriteLine($"{GetType().Name} is turned off");
        }
    }

    public class Switch
    {
        private ISwitchable device;

        public Switch(ISwitchable device)
        {
            this.device = device;
        }

        public void Operate()
        {
            // Operate switch logic
            if ((new Random()).Next(0, 2) == 0)
            {
                device.TurnOn();
            }
            else
            {
                device.TurnOff();
            }
        }
    }

}
